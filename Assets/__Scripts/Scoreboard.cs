﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// The Scoreboard is a scoreboard. It does exactly what you think it does.
public class Scoreboard : MonoBehaviour {
	public static Scoreboard S; // The singleton for Scoreboard

	public GameObject prefabFloatingScore;

	[HideInInspector]
	public GameObject ScoreCounter;
	[HideInInspector]
	public GameObject HighScoreCounter;
	[HideInInspector]
	public GameObject GameOverDisplay;
	[HideInInspector]
	public GameObject RoundResult;

	[Header("----------------")]

	[SerializeField]
	private int		_score = 0;
	private string	_scoreString;

	// The score property also sets the scoreString
	public int score {
		get {
			return (_score);
		}
		set {
			_score = value;
			scoreString = Utils.AddCommasToNumber (_score);
		}
	}

	// The scoreString property also sets what was the GUIText.text, though that's now depreciated
	public string scoreString {
		get {
			return (_scoreString);
		}
		set {
			_scoreString = value;
			ScoreCounter.GetComponent<Text>().text = "Score: " + _scoreString;
			GetComponent<AudioSource> ().Play ();
		}
	}

	void Awake () {
		S = this;
	}

	void Start() {
		ScoreCounter = gameObject.transform.Find ("ScoreCounter").gameObject;
		HighScoreCounter = gameObject.transform.Find ("HighScore").gameObject;
		GameOverDisplay = gameObject.transform.Find ("GameOver").gameObject;
		RoundResult = gameObject.transform.Find ("RoundResult").gameObject;

		HighScoreCounter.GetComponent<Text>().text = "High Score: " + Prospector.HIGH_SCORE;
	}

	public void GameOver(bool win) {
		GameOverDisplay.SetActive (true);
		RoundResult.SetActive (true);
		if (win) {
			GameOverDisplay.GetComponent<Text>().text = "You Win!";
		} else {
			GameOverDisplay.GetComponent<Text>().text = "Game Over";
		}

		if (Prospector.HIGH_SCORE < Prospector.S.score) {
			RoundResult.GetComponent<Text>().text = "You got the new High Score!\nHigh Score: " + Prospector.S.score;
		} else {
			RoundResult.GetComponent<Text>().text = "Your score: " + Prospector.S.score;
		}
	}

	// When called by SendMessage, this adds the fs.score
	public void FSCallback(FloatingScore fs) {
		score += fs.score;
	}

	// This will Instantiate a new FloatingScore GameObject and initialize it.
	// It also returns a pointer to the FloatingScore created so that the
	// calling function can do more with it (like set fontSizes, etc.)
	public FloatingScore CreateFloatingScore(int amt, List<Vector3> pts) {
		GameObject go = Instantiate (prefabFloatingScore) as GameObject;
		go.transform.SetParent (this.transform);
		FloatingScore fs = go.GetComponent<FloatingScore> ();
		fs.score = amt;
		fs.reportFinishTo = this.gameObject; // Set fs to call back to this
		fs.Init(pts);
		return(fs);
	}
}
